@extends('layout.master')
@section('Hallo')
 Sign Up Form 
@endsection

@section('content')
<h2>Buat Account Baru</h2>
<form action="/kirim" method="POST">
    @csrf
    <label>First Name :</label><br>
    <input type="text" name="name"><br><br>
    <label>Last Name :</label><br>
    <input type="text" name="lastName"><br><br>
    <label> Gender :</label><br>
    <p><input type='radio' name='Gender' value='male' />Male</p>
    <p><input type='radio' name='Gender' value='female' />Female</p>
    <p><input type='radio' name='Gender' value='other' />other</p>
    <label>Nationality :</label>
    
    <select name="country">
        <option value="Indonesia">Indonesia</option>
        <option value="America">America</option>
        <option value="Inggris">Inggris</option>
    </select><br><br>
    
    <label>Language Spoken</label><br><br>
    <input type="checkbox" name="Language" value='Bahasa Indonesia' />Bahasa Indonesia<br>
    <input type="checkbox" name="Language" value='English' />English<br>
    <input type="checkbox" name="Language" value='Arabic' />Arabic<br>
    <input type="checkbox" name="Language" value='Japanese' />Japanese<br><br>

    <label>Bio</label><br><br>
    <textarea name="message" rows="10" cols="30"></textarea>
    <br><br>
    <input type="submit" value="Sign Up">

    @endsection
