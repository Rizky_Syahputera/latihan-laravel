<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/table' , function(){
    return view('table');
});

Route::get('/data-tables', function(){
    return view ('data-table');
});

Route::get('/master', function(){
    return view ('layout.master');
});

Route::get('/', 'IndexController@dashboard');
Route::get('/register', 'RegisterController@registrasi');

Route::post('/kirim', 'RegisterController@welcome');

//CRUD kategori
// -> form create data
Route::get('/kategori/create', 'KategoriController@create');
// menyimpan data ke table kategori
Route::post('/kategori', 'KategoriController@store');
// tampil semua data
Route::get('/kategori', 'KategoriController@index');
//detail data
Route::get('/kategori/{kategori_id}', 'KategoriController@show');
//-> ke form edit data
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
//-> Update data ke dalam table kategori
Route::put('/kategori/{kategori_id}', 'KategoriController@update');
// delete data
Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');
