<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    public function create()
    {
        return view('kategori.create');
    }

    public function store(Request $request){
       $request->validate(
            [
                     'nama' => 'required',
                     'deskripsi' => 'required',
                    
            ],
            [
                     'nama.required' => 'Input Nama Harus di isi!',
                     'deskripsi.required'  => 'Deskripsi Harus di isi!',
            ]
    
        );
        DB::table('kategori')->insert(
            [
                'nama' => $request['nama'],
                'deskripsi' => $request['deskripsi']
            ]
        );
        return redirect('/kategori');
    }

    public function index()
    {
        $kategori = DB::table('kategori')->get();
 
        return view('kategori.index', compact('kategori'));
    }

    public function show($id)
    {
        $kategori = $user = DB::table('kategori')->where('id', $id)->first();
        
        return view('kategori.show', compact('kategori'));
    }
    public function edit($id)
    {
        $kategori = $user = DB::table('kategori')->where('id', $id)->first();
        
        return view('kategori.edit', compact('kategori'));
    }

    public function update($id, Request $request)
    {
        $request->validate(
            [
                     'nama' => 'required',
                     'deskripsi' => 'required',
                    
            ],
            [
                     'nama.required' => 'Input Nama Harus di isi!',
                     'deskripsi.required'  => 'Deskripsi Harus di isi!',
            ]
    
        );

        DB::table('kategori')->where('id', $id)->update(
            [
                'nama' => $request['nama'],
                'deskripsi' => $request['deskripsi'],
            
            ]
        );
        return redirect('/kategori');
    }

    public function destroy($id)
    {
         DB::table('kategori')->where('id', '=', $id)->delete();

         return redirect('/kategori');
    }
   



}

