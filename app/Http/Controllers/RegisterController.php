<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function registrasi()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        // dd($request->all());
        $name = $request['name'];
        $lastName = $request['lastName'];

        return view('welcome', compact('name' , 'lastName'));
    }
}
